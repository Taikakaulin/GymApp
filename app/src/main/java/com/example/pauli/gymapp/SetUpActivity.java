package com.example.pauli.gymapp;

import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import android.widget.EditText;
import android.widget.Button;
import android.app.Activity;
import android.view.View;
import java.util.Date;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;
import android.view.View.OnClickListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SetUpActivity extends AppCompatActivity {
    private DatabaseReference mDatabase;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_up);
        mDatabase = FirebaseDatabase.getInstance().getReference();

       final EditText editReps = (EditText)findViewById(R.id.editText);
        final EditText editWeight = (EditText)findViewById(R.id.editTextWeight);

       final Spinner spinner = (Spinner) findViewById(R.id.spinner);

        //database reference pointing to demo node

        // Spinner element

        /*
        Spinner click listener
        Spinner Drop down elements
        */


        List<String> categories = new ArrayList<String>();
        categories.add("Flat Barbell Bench Press");
        categories.add("Incline Barbell Bench Press");
        categories.add("Chest Flyes");
        categories.add("Pullups");
        categories.add("Chinups");
        categories.add("Deadlifts");
        categories.add("Military Press");
        categories.add("Dumbbell Overhead Press (Arnold/Scott Press)");
        categories.add("Barbell Curls");
        categories.add("Dumbbell Curls");
        categories.add("Incline Dumbbell Curls");
        categories.add("Standing French Press");
        categories.add("Skullcrushers");
        categories.add("Close Grip Bench Press");
        categories.add("Squats");
        categories.add("Leg Press");







        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);







        spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                Object item = adapterView.getItemAtPosition(position);
                if (item != null) {
                    Toast.makeText(SetUpActivity.this, item.toString(),
                            Toast.LENGTH_SHORT).show();
                }
                Toast.makeText(SetUpActivity.this, "Selected",
                        Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // TODO Auto-generated method stub

            }
        });
        Button button = (Button)findViewById(R.id.button);







        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                Date today = Calendar.getInstance().getTime();
                final String reportDate = df.format(today);
                String text = spinner.getSelectedItem().toString();
                String value = editReps.getText().toString();
                String val = editWeight.getText().toString();
                //push creates a unique id in database
                Date date = new Date();
                mDatabase.child(reportDate).child("Move").setValue(text);
                mDatabase.child(reportDate).child("weight").setValue(val);
                mDatabase.child(reportDate).child("Reps").setValue(value);

            }
        });
    }

}




